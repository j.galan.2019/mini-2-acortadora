from django.db import models


class Urls_DataBase(models.Model):
    # Parameters of the Short Url
    short = models.CharField(max_length=32)
    url = models.TextField()

    # String of the Short Url
    def __str__(self):
        return self.short
