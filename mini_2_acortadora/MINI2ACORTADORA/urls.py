from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<str:recurso>', views.get_recurso, name='get_recurso'),
]