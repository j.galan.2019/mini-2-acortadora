from django.shortcuts import render, redirect
from .models import Urls_DataBase
from django.views.decorators.csrf import csrf_exempt


def get_counter():
    contador = 1
    existe = False
    if Urls_DataBase.objects.count() != 0:
        while not existe:
            search_url = Urls_DataBase.objects.filter(short=str(contador))
            if search_url:
                contador += 1
            else:
                existe = True
    return contador

def get_recurso(request, recurso):
    try:
        short_url = Urls_DataBase.objects.get(short=recurso)
        return redirect(short_url.url)
    except Urls_DataBase.DoesNotExist:
        url_dic = Urls_DataBase.objects.all()
        return render(request, "acortadora/Error.html", {'recurso':recurso,'url_dic': url_dic})


@csrf_exempt
def index(request):
    if request.method == "GET":
        url_dic = Urls_DataBase.objects.all()
        return render(request, "acortadora/Page.html", {'url_dic': url_dic})
    elif request.method == "POST":
        url_dic = Urls_DataBase.objects.all()
        sh_url = request.POST["url"]
        url = Urls_DataBase.objects.filter(url__endswith=sh_url)
        if not url:
            if 'http://' not in sh_url and 'https://' not in sh_url:
                sh_url = 'http://' + sh_url
            if not request.POST["short"]:
                contador = get_counter()
                url = Urls_DataBase(short=str(contador), url=sh_url)
                url.save()
            else:
                url = Urls_DataBase(short=request.POST["short"], url=sh_url)
                url.save()
        return render(request, "acortadora/Page.html", {'url_dic': url_dic})




