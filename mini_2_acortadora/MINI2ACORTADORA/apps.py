from django.apps import AppConfig


class Mini2AcortadoraConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "MINI2ACORTADORA"
